WITH depts_sals_avg AS(
  SELECT 
    e.*, 
    AVG(e.salary) OVER(PARTITION BY e.dept_id )  AS avg_sal_dept
  FROM EMP e
)
SELECT 
  dsa.lname
FROM depts_sals_avg dsa
WHERE dsa.salary > dsa.avg_sal_dept;  
