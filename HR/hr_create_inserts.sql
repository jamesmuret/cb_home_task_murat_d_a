

drop table DEPT cascade constraints;
drop table EMP cascade constraints;
create table DEPT
(
  dept_id NUMBER(5) not null,
  name    VARCHAR2(30)
)
;
comment on column DEPT.dept_id
  is '���������� ������������� �������������';
comment on column DEPT.name
  is '������������ �������������';
alter table DEPT
  add constraint DEPT_ID_PK primary key (DEPT_ID);
create table EMP
(
  emp_id  NUMBER(5) not null,
  fname   VARCHAR2(30),
  lname   VARCHAR2(30),
  dept_id NUMBER(5),
  salary  NUMBER(8,2)
)
;
comment on column EMP.emp_id
  is '���������� ������������� ���������� ';
comment on column EMP.fname
  is '��� ����������';
comment on column EMP.lname
  is '������� ����������';
comment on column EMP.dept_id
  is '������������� �������������, ��� �������� ���������';
comment on column EMP.salary
  is '�������� ����������';
alter table EMP
  add constraint EMP_ID_PK primary key (EMP_ID);
alter table EMP
  add constraint DEPT_ID_FK foreign key (DEPT_ID)
  references DEPT (DEPT_ID) on delete set null;

alter table DEPT disable all triggers;
alter table EMP disable all triggers;
alter table EMP disable constraint DEPT_ID_FK;
insert into DEPT (dept_id, name)
values (20, 'Marketing');
insert into DEPT (dept_id, name)
values (30, 'Purchasing');
insert into DEPT (dept_id, name)
values (40, 'Human Resources');
insert into DEPT (dept_id, name)
values (50, 'Shipping');
insert into DEPT (dept_id, name)
values (60, 'IT');
insert into DEPT (dept_id, name)
values (70, 'Public Relations');
insert into DEPT (dept_id, name)
values (80, 'Sales');
insert into DEPT (dept_id, name)
values (90, 'Executive');
insert into DEPT (dept_id, name)
values (100, 'Finance');
insert into DEPT (dept_id, name)
values (110, 'Accounting');
insert into DEPT (dept_id, name)
values (120, 'Treasury');
insert into DEPT (dept_id, name)
values (130, 'Corporate Tax');
insert into DEPT (dept_id, name)
values (140, 'Control And Credit');
insert into DEPT (dept_id, name)
values (150, 'Shareholder Services');
insert into DEPT (dept_id, name)
values (160, 'Benefits');
insert into DEPT (dept_id, name)
values (170, 'Manufacturing');
insert into DEPT (dept_id, name)
values (180, 'Construction');
insert into DEPT (dept_id, name)
values (190, 'Contracting');
insert into DEPT (dept_id, name)
values (200, 'Operations');
insert into DEPT (dept_id, name)
values (210, 'IT Support');
insert into DEPT (dept_id, name)
values (220, 'NOC');
insert into DEPT (dept_id, name)
values (230, 'IT Helpdesk');
insert into DEPT (dept_id, name)
values (240, 'Government Sales');
insert into DEPT (dept_id, name)
values (250, 'Retail Sales');
insert into DEPT (dept_id, name)
values (260, 'Recruiting');
insert into DEPT (dept_id, name)
values (270, 'Payroll');
insert into DEPT (dept_id, name)
values (10, 'Administration');
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (100, 'Steven', 'King', 90, 24000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (101, 'Neena', 'Kochhar', 90, 17000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (102, 'Lex', 'De Haan', 90, 17000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (103, 'Alexander', 'Hunold', 60, 9000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (104, 'Bruce', 'Ernst', 60, 6000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (105, 'David', 'Austin', 60, 4800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (106, 'Valli', 'Pataballa', 60, 4800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (107, 'Diana', 'Lorentz', 60, 4200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (108, 'Nancy', 'Greenberg', 100, 12000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (109, 'Daniel', 'Faviet', 100, 9000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (110, 'John', 'Chen', 100, 8200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (111, 'Ismael', 'Sciarra', 100, 7700);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (112, 'Jose Manuel', 'Urman', 100, 7800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (113, 'Luis', 'Popp', 100, 6900);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (114, 'Den', 'Raphaely', 30, 11000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (115, 'Alexander', 'Khoo', 30, 3100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (116, 'Shelli', 'Baida', 30, 2900);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (117, 'Sigal', 'Tobias', 30, 2800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (118, 'Guy', 'Himuro', 30, 2600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (119, 'Karen', 'Colmenares', 30, 2500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (120, 'Matthew', 'Weiss', 50, 8000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (121, 'Adam', 'Fripp', 50, 8200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (122, 'Payam', 'Kaufling', 50, 7900);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (123, 'Shanta', 'Vollman', 50, 6500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (124, 'Kevin', 'Mourgos', 50, 5800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (125, 'Julia', 'Nayer', 50, 3200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (126, 'Irene', 'Mikkilineni', 50, 2700);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (127, 'James', 'Landry', 50, 2400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (128, 'Steven', 'Markle', 50, 2200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (129, 'Laura', 'Bissot', 50, 3300);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (130, 'Mozhe', '2800', null, 50);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (131, 'James', 'Marlow', 50, 2500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (132, 'TJ', 'Olson', 50, 2100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (133, 'Jason', 'Mallin', 50, 3300);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (134, 'Michael', 'Rogers', 50, 2900);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (135, 'Ki', 'Gee', 50, 2400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (136, 'Hazel', 'Philtanker', 50, 2200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (137, 'Renske', 'Ladwig', 50, 3600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (138, 'Stephen', 'Stiles', 50, 3200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (139, 'John', 'Seo', 50, 2700);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (140, 'Joshua', 'Patel', 50, 2500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (141, 'Trenna', 'Rajs', 50, 3500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (142, 'Curtis', 'Davies', 50, 3100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (143, 'Randall', 'Matos', 50, 2600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (144, 'Peter', 'Vargas', 50, 2500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (145, 'John', 'Russell', 80, 14000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (146, 'Karen', 'Partners', 80, 13500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (147, 'Alberto', 'Errazuriz', 80, 12000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (148, 'Gerald', 'Cambrault', 80, 11000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (149, 'Eleni', 'Zlotkey', 80, 10500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (150, 'Peter', 'Tucker', 80, 10000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (151, 'David', 'Bernstein', 80, 9500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (152, 'Peter', '9000', null, 80);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (153, 'Christopher', 'Olsen', 80, 8000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (154, 'Nanette', 'Cambrault', 80, 7500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (155, 'Oliver', 'Tuvault', 80, 7000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (156, 'Janette', 'King', 80, 10000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (157, 'Patrick', 'Sully', 80, 9500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (158, 'Allan', 'McEwen', 80, 9000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (159, 'Lindsey', 'Smith', 80, 8000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (160, 'Louise', 'Doran', 80, 7500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (161, 'Sarath', 'Sewall', 80, 7000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (162, 'Clara', 'Vishney', null, 80);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (163, 'Danielle', 'Greene', 80, 9500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (164, 'Mattea', 'Marvins', 80, 7200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (165, 'David', 'Lee', 80, 6800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (166, 'Sundar', 'Ande', 80, 6400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (167, 'Amit', 'Banda', 80, 6200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (168, 'Lisa', 'Ozer', 80, 11500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (169, 'Harrison', 'Bloom', 80, 10000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (170, 'Tayler', 'Fox', 80, 9600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (171, 'William', 'Smith', 80, 7400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (172, 'Elizabeth', 'Bates', 80, 7300);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (173, 'Sundita', 'Kumar', 80, 6100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (174, 'Ellen', 'Abel', 80, 11000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (175, 'Alyssa', 'Hutton', 80, 8800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (176, 'Jonathon', 'Taylor', 80, 8600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (177, 'Jack', 'Livingston', 80, 8400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (178, 'Kimberely', 'Grant', null, 7000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (179, 'Charles', 'Johnson', 80, 6200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (180, 'Winston', 'Taylor', 50, 3200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (181, 'Jean', 'Fleaur', 50, 3100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (182, 'Martha', 'Sullivan', 50, 2500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (183, 'Girard', 'Geoni', 50, 2800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (184, 'Nandita', 'Sarchand', 50, 4200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (185, 'Alexis', 'Bull', 50, 4100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (186, 'Julia', 'Dellinger', null, 3400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (187, 'Anthony', 'Cabrio', 50, 3000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (188, 'Kelly', 'Chung', 50, 3800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (189, 'Jennifer', 'Dilly', 50, 3600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (190, 'Timothy', 'Gates', 50, 2900);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (191, 'Randall', 'Perkins', 50, 2500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (192, 'Sarah', 'Bell', 50, 4000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (193, 'Britney', 'Everett', 50, 3900);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (194, 'Samuel', 'McCain', 50, 3200);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (195, 'Vance', 'Jones', 50, 2800);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (196, 'Alana', 'Walsh', 50, 3100);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (197, 'Kevin', 'Feeney', 50, 3000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (198, 'Donald', 'OConnell', 50, 2600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (199, 'Douglas', 'Grant', 50, 2600);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (200, 'Jennifer', 'Whalen', 10, 4400);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (201, 'Michael', 'Hartstein', 20, 13000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (202, 'Pat', 'Fay', 20, 6000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (203, 'Susan', 'Mavris', 40, 6500);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (204, 'Hermann', 'Baer', 70, 10000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (205, 'Shelley', 'Higgins', 110, 12000);
insert into EMP (emp_id, fname, lname, dept_id, salary)
values (206, 'William', 'Gietz', 110, 8300);
alter table EMP enable constraint DEPT_ID_FK;
alter table DEPT enable all triggers;
alter table EMP enable all triggers;

