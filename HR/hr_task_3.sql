WITH emp_sals_dept_ranked AS( 
SELECT 
  e.lname,
  DENSE_RANK() 
  OVER(PARTITION BY e.dept_id ORDER BY e.salary DESC) salary_dep_rank
FROM EMP e
ORDER BY e.dept_id NULLS LAST, salary_dep_rank asc
)
SELECT 
  sd.lname
FROM emp_sals_dept_ranked sd
WHERE sd.salary_dep_rank = 1;
