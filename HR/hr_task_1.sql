SELECT 
  d.name
FROM DEPT d
WHERE NOT EXISTS (SELECT 1 FROM EMP e WHERE e.dept_id = d.dept_id);
