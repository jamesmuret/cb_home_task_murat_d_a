-- ������������ ����� ������� ��� ����, ����� ��������� ������������� ��������� �������������
-- � �������� ��������� ������������ ������� ���� ���� ���
WITH lnames_doubles AS(
SELECT 
  e.lname,
  e.fname,
  e.dept_id,
  ee.lname AS lname_double,
  ee.fname AS fname_double,
  ee.dept_id AS dept_id_double,
  DENSE_RANK() OVER(PARTITION BY e.lname ORDER BY e.emp_id) AS lname_rank
FROM EMP e
INNER JOIN EMP ee
ON e.dept_id <> ee.dept_id
AND e.lname = ee.lname
)
SELECT 
  ld.lname,
  ld.fname,
  ld.dept_id,
  lname_double,
  fname_double,
  dept_id_double
FROM lnames_doubles ld
WHERE ld.lname_rank = 1;

