SELECT 
  d.name,
  COUNT(e.emp_id) AS emp_cnt
FROM DEPT d
INNER JOIN EMP e
ON d.dept_id = e.dept_id
GROUP BY(d.name)
HAVING COUNT(e.emp_id) >3
ORDER BY emp_cnt asc;
