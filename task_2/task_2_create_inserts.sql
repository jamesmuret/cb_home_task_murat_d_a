
drop table TASK_2_1_CLIENT cascade constraints;
drop table TASK_2_1_TRANSACTIONS cascade constraints;
drop table TASK_2_2 cascade constraints;
drop table TASK_2_4 cascade constraints;
drop table TASK_2_5_REPORT_DATES cascade constraints;
drop table TASK_2_5_REST_RUB cascade constraints;
create table TASK_2_1_CLIENT
(
  id   NUMBER,
  name VARCHAR2(200)
)
;

create table TASK_2_1_TRANSACTIONS
(
  date_t    DATE,
  qty       NUMBER(6,2),
  client_id NUMBER
)
;
comment on table TASK_2_1_TRANSACTIONS
  is '���������� ��������';

create table TASK_2_2
(
  account_id NUMBER,
  oper_date  DATE,
  oper_qty   NUMBER(6,2)
)
;
comment on column TASK_2_2.account_id
  is '���������� ����';
comment on column TASK_2_2.oper_date
  is '���� �������';
comment on column TASK_2_2.oper_qty
  is '����������';

create table TASK_2_4
(
  column_one VARCHAR2(50)
)
;
comment on table TASK_2_4
  is '�������� ����������';

create table TASK_2_5_REPORT_DATES
(
  date_report DATE
)
;

create table TASK_2_5_REST_RUB
(
  date_rest DATE,
  rest_458  NUMBER(6,2)
)
;
comment on table TASK_2_5_REST_RUB
  is '������� ������������� �� ��������� ����� �������';
comment on column TASK_2_5_REST_RUB.date_rest
  is '���� ������������� �������������';
comment on column TASK_2_5_REST_RUB.rest_458
  is '������������� � ������';

alter table TASK_2_1_CLIENT disable all triggers;
alter table TASK_2_1_TRANSACTIONS disable all triggers;
alter table TASK_2_2 disable all triggers;
alter table TASK_2_4 disable all triggers;
alter table TASK_2_5_REPORT_DATES disable all triggers;
alter table TASK_2_5_REST_RUB disable all triggers;
insert into TASK_2_1_CLIENT (id, name)
values (1, '����');
insert into TASK_2_1_CLIENT (id, name)
values (2, '����');
insert into TASK_2_1_CLIENT (id, name)
values (3, '����');
insert into TASK_2_1_CLIENT (id, name)
values (4, '����');
commit;
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('10-01-2010', 'dd-mm-yyyy'), 1000, 1);
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('10-01-2010', 'dd-mm-yyyy'), 100, 4);
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('13-03-2010', 'dd-mm-yyyy'), 400, 2);
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('14-03-2010', 'dd-mm-yyyy'), 130, 4);
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('15-04-2010', 'dd-mm-yyyy'), 1100, 2);
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('17-04-2010', 'dd-mm-yyyy'), 1100, 3);
insert into TASK_2_1_TRANSACTIONS (date_t, qty, client_id)
values (to_date('19-04-2010', 'dd-mm-yyyy'), 110, 3);
commit;
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('10-02-2010', 'dd-mm-yyyy'), 100);
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('12-01-2010', 'dd-mm-yyyy'), 800);
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('01-06-2010', 'dd-mm-yyyy'), 2500);
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('15-04-2010', 'dd-mm-yyyy'), 400);
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('10-02-2010', 'dd-mm-yyyy'), 4000);
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('01-05-2010', 'dd-mm-yyyy'), 100);
insert into TASK_2_2 (account_id, oper_date, oper_qty)
values (1, to_date('01-06-2010', 'dd-mm-yyyy'), 150);
commit;
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-12-2009', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-01-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('28-02-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-03-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('30-04-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-05-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('30-06-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-07-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-08-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('30-09-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-10-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('30-11-2010', 'dd-mm-yyyy'));
insert into TASK_2_5_REPORT_DATES (date_report)
values (to_date('31-12-2010', 'dd-mm-yyyy'));
commit;
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('12-01-2010', 'dd-mm-yyyy'), 100);
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('10-02-2010', 'dd-mm-yyyy'), 800);
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('15-04-2010', 'dd-mm-yyyy'), 0);
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('11-09-2010', 'dd-mm-yyyy'), 400);
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('01-06-2010', 'dd-mm-yyyy'), 0);
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('01-07-2010', 'dd-mm-yyyy'), 0);
insert into TASK_2_5_REST_RUB (date_rest, rest_458)
values (to_date('12-07-2010', 'dd-mm-yyyy'), 300);
commit;
alter table TASK_2_1_CLIENT enable all triggers;
alter table TASK_2_1_TRANSACTIONS enable all triggers;
alter table TASK_2_2 enable all triggers;
alter table TASK_2_4 enable all triggers;
alter table TASK_2_5_REPORT_DATES enable all triggers;
alter table TASK_2_5_REST_RUB enable all triggers;
