WITH base_dates AS(
  SELECT 
    tt.date_report,
    nvl((SELECT MAX(t.date_rest) FROM task_2_5_rest_rub t
     WHERE t.rest_458=0 AND t.date_rest <= tt.date_report),
     (SELECT MIN(r.date_rest) FROM task_2_5_rest_rub r))
      max_null_date,
    (SELECT MAX(t.date_rest) FROM task_2_5_rest_rub t
     WHERE t.rest_458 <>0 AND t.date_rest <= tt.date_report) max_not_null_date
  FROM task_2_5_report_dates tt
), min_dates_base AS(
  SELECT
    b.*,
    (SELECT MIN(t.date_rest) FROM task_2_5_rest_rub t
     WHERE t.date_rest >= b.max_null_date 
     AND t.rest_458 <>0) min_not_null_date_after_null
  FROM base_dates b
)
SELECT 
  bb.date_report,
  CASE 
    WHEN bb.max_not_null_date >= bb.max_null_date 
      AND bb.max_not_null_date IS NOT NULL
    THEN bb.date_report - bb.min_not_null_date_after_null
    ELSE 0 END days_to_report     
FROM min_dates_base bb;



  
    
