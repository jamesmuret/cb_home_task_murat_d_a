-- ������ ������� � ������� �������
-- ����������� ����� ��������� ���������� � ������� ���������� ��������� ���������� �� �����
SELECT 
  c.id,
  c.name,
  SUM(t.qty) 
  OVER(PARTITION BY t.client_id ORDER BY t.date_t asc) amnt_transactions,
  t.date_t
FROM task_2_1_transactions t
LEFT JOIN task_2_1_Client c
ON c.id = t.client_id
ORDER BY t.date_t asc
