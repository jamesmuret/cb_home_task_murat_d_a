
-- ������ ���������� ���������� �����, � ���������� ������������ ���� � ������� ����� �� ��������� ����
-- ������ �� ������ ������� � ���� ���� ����� ���� ��������� ���������� - ����� ������� �� �����
with max_dates_accounts AS(
select 
  t.account_id, 
  MAX(t.oper_date) max_date
from task_2_2 t
group by t.account_id
), account_max_date_sum AS(
SELECT  tt.account_id,
        tt.oper_date,
        SUM(tt.oper_qty) sum_max_date
FROM task_2_2 tt
INNER JOIN max_dates_accounts mda
ON tt.oper_date = mda.max_date
AND tt.account_id = mda.account_id
GROUP BY tt.account_id,tt.oper_date 
)
SELECT *
FROM account_max_date_sum




